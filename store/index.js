export const state = () => ({
  asideRight: false,
  settingsPopUp: false,
  contactsPopUp: false,
  archivePopUp: false,
  searchPopUp: false,
  hubList: [],
  userList: [
    // {
    //   nickName: "Saved messages",
    //   lastMsg: "Qale",
    //   avatarImg: require("@/assets/media/image/avatar0.JPEG"),
    //   uniqueName: "@akhmad",
    // },
    {
      nickName: "Abdulloh",
      avatarImg: require("~/assets/media/image/avatar2.jpg"),
      uniqueName: "Abdulloh_2004",
      phoneNumber: "(77) 777-77-77",
      bio: 'Im am Businessman',
    },
    {
      nickName: "Ibrohim",
      avatarImg: require("~/assets/media/image/avatar3.jpeg"),
      uniqueName: "no0ne",
      phoneNumber: "(98) 333-23-03",
      bio: 'Im am Backend developer',
    },
    {
      nickName: "Muhammad",
      avatarImg: require("~/assets/media/image/avatar4.jpeg"),
      uniqueName: "muhammad_05",
      phoneNumber: "(01) 010-10-10",
      bio: 'Im am Frilancer',
    },
    {
      nickName: "Vlad",
      avatarImg: require("~/assets/media/image/avatar1.jpeg"),
      uniqueName: "vlad_65",
      phoneNumber: "+7 123-4567-901",
      bio: 'Im am Businessman',
    },
  ],
  chatList: [
    {
      users: ["muhammad_05", "no0ne"],
      messages: [
        {
          uniqueName: "muhammad_05",
          text: 'Hello',
          postDate: '12:02'
        },
        {
          uniqueName: "no0ne",
          text: 'Hi',
          postDate: '11:52'
        }
      ],
    }
  ]

  //   menuList: [],
  //   newsList: [],
  //   partnerList: [],
  //   productsList: [],
  //   instructionsList: [],
  //   managementsList: [],
  //   advantagesList: [],
  //   numbersList: [],
  //   contactsList: [],
  //   galleryVideoList: [],
  //   galleryPhotoList: [],
  //   regionList: [],
  //   apparatsList: [],
});
export const getters = {
  settingsPopUp: (state) => {
    return state.settingsPopUp;
  },
  contactsPopUp: (state) => {
    return state.contactsPopUp;
  },
  archivePopUp: (state) => {
    return state.archivePopUp;
  },
  searchPopUp: (state) => {
    return state.searchPopUp;
  },

  asideRight: (state) => {
    return state.asideRight;
  },

  userList: (state) => {
    return state.userList;
  },
  contactsList: (state, getters) => {
    return state?.userList?.filter(user => user?.uniqueName != getters.getToken);
  },
  mySelf: (state, getters) => {
    return state?.userList?.find(user => user?.uniqueName == getters.getToken) || {};
  },
  myChats: (state, getters) => {
    let arr = state.chatList.filter(chat => chat?.users?.includes(getters.getToken)) || []
    return arr.map(e => ({
      ...e,
      partnerToken: e.users.find((user) => user != getters.getToken),
      partner() {
        const partnerToken = this.partnerToken
        let user = state?.userList?.find(e => e.uniqueName == partnerToken)
        user.lastMsg = () => e?.messages?.at(-1) 
        return user;
      },
    }));
  },
  currentOpenedChat: (state, getters) => (token) => {
    if (token) {
      return getters?.myChats?.find(e => e?.partnerToken == token)
    } else {
      return null;
    }
  },
  hubList: (state) => {
    return state.hubList;
  },
  getToken: () => sessionStorage?.getItem("token")
};

export const mutations = {
  setState(state, { key, payload }) {
    state[key] = payload;
  },
  changeAsideRight(state, value) {
    state.asideRight = value;
  },
  changePopUp(state, { key, value }) {
    state[key] = value;
  },
  setMessage(state, {message, partnerToken}) {
    if (partnerToken) {
      let chat = state?.chatList?.find(e => e.users.includes(partnerToken, getters.getToken() ))
      if (chat) {
        chat?.messages?.push({...message, uniqueName: getters.getToken()})
      } else {
        state?.chatList?.push({
          users: [partnerToken, getters.getToken()],
          messages: [{...message, uniqueName: getters.getToken() }]
        })
      }
    }
  },
  deleteChatFormChatList(state, partnerToken) {
    if (partnerToken) {
      let chatIndex = state?.chatList?.findIndex(e => e.users.includes(partnerToken, getters.getToken() ))
      state.chatList?.splice(chatIndex, 1)
    }
  }
};
export const actions = {
  fetchListDefault({ commit }, { api, key, params }) {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await this.$axios.get(`/${api}`, {
          headers: {
            Authorization: params.token,
          },
        });
        const { status, data, message } = res.data;
        if (status) {
          commit("setState", {
            key,
            payload: data.results || data || res || [],
          });
          resolve({
            status,
            data,
          });
        } else {
          reject(message);
        }
      } catch (error) {
        reject(error);
      }
    });
  },
  sendMessage({ commit }, obj) {
    commit('setMessage', obj)
  },
  deleteChat({commit}, partnerToken) {
    commit('deleteChatFormChatList', partnerToken)
  }
};
