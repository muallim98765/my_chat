export const state = () => ({
  auth: false,
  token: null,
});
export const getters = {
  auth: (state) => {
    return state.auth;
  },
  token: (state) => {
    return state.token;
  },
};

export const mutations = {
  setState(state, { key, payload }) {
    state[key] = payload;
  },
  setAuth(state, boolean) {
    state.auth = boolean;
  },
};
export const actions = {
  signUp({ dispatch }, form) {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await this.$axios.post(
          `http://192.168.1.77:8080/SignUp`,
          form
        );
        if (data && data.status) {
          resolve({ error: false, status: true });
          dispatch("signIn", data.data);
        } else {
          reject({ error: true, status: false });
        }
      } catch (error) {
        reject({
          error: true,
          status: false,
        });
      }
    });
  },
  signIn({ commit }, form) {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await this.$axios.post(
          `http://192.168.1.77:8080/SignIn`,
          form
        );
        if (data && data.status) {
          sessionStorage.setItem("token", data.data.token);

          const token = `Bearer ${data.data.token}`;
          commit("setState", { key: "token", payload: token });
          commit("setAuth", true);

          resolve({ error: false, status: true });

          this.$router.push("/");
        } else {
          reject({ error: true, status: false });
        }
      } catch (error) {
        reject({
          error: true,
          status: false,
        });
      }
    });
  },
  async signInWithAcc({ commit }, form) {
    sessionStorage.setItem("token", form.uniqueName);
    const token = `Bearer ${form.uniqueName}`;
    commit("setState", { key: "token", payload: token });
    this.$router.push("/");
  }
};
