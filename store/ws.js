export const state = () => ({
  ws: null,
  message: "",
  messagesList: [],
});

export const getters = {
  messagesList: (state) => {
    return state.messagesList;
  },
  ws: (state) => {
    return state.ws;
  },
};

export const mutations = {
  setMessage(state, message) {
    state.messagesList.push(message);
  },
  openServer(state, { ws }) {
    state.ws = ws;
  },
};

export const actions = {
  openServer({ commit }) {
    return new Promise(async (resolve, reject) => {
      try {
        const ws = new WebSocket(`ws://192.168.1.77:8080/hub/ws`);
        commit("openServer", { ws });
      } catch (error) {
        reject(error);
      }
    });
  },
  receiveMessage({ state, commit }, obj) {
    const list = JSON.parse(obj);
    list.type = 1;
    commit("setMessage", list);
  },
  sendMessage({ state, commit }, obj) {
    // state.ws.send(JSON.stringify(obj));
    commit("setMessage", obj);
  },
};

// export const created = () => {
//   this.$websocket = createWebSocket({
//     url: "ws://localhost:8080/ws",
//     events: {
//       message: this.receiveMessage,
//     },
//   });
// };
