const ws = require("ws");
const server = new ws.Server({ port: 8080 });
server.on("connection", (socket) => {
  console.log("New connection established");
  // Send a message to the client
  // socket.send("Welcome to the server!");
  socket.on("message", (message) => {
    console.log("Received message: " + message);
    socket.send("" + message);
  });
});
