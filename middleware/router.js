export default function ({ store, route, redirect }) {
  const token = sessionStorage.getItem("token");
  if (token) {
    store.commit("apply/setState", {
      key: "token",
      payload: `Bearer ${token}`,
    });
    store.commit("apply/setAuth", true);
  }

  if (
    !store.state.apply.auth &&
    !store.state.apply.token &&
    route.path != "/login"
  ) {
    redirect("/login");
  }
}
