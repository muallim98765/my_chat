export default function ({ store, redirect }) {
  if (!store.state.apply.auth) {
    redirect("/login");
  }
}
