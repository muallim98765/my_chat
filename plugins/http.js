export default async (context, inject) => {
  inject("http", function ({ method = "get", url, payload, formData, params }) {
    return new Promise(async (resolve, reject) => {
      try {
        let options = { ...payload };
        if (params) {
          options = { ...options, params };
        }
        let i18n = context.app.i18n.locale;
        const res = await context.$axios[method](
          url,
          formData ? formData : options
        );
        // console.log(res)
        const { status, message, data } = (res && res.data) || {};
        // console.log(data)
        if (!status) {
          reject((message && message[i18n]) || message);
          return;
        }
        resolve(data);
      } catch (e) {
        reject(e && e.response && e.response.data && e.response.data.error);
      }
    });
  });
};
