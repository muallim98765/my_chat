export default ({ store }) => {
  // Function to sync localStorage with Vuex store
  const syncLocalStorage = () => {
    const chatList = JSON.parse(localStorage.getItem('chatList'));
    if (chatList) {
      store.commit('setState', {
				key: 'chatList',
				payload: chatList
			});
    }
  };

  // Initial synchronization
  syncLocalStorage();

  // Watch for changes in localStorage
  window.addEventListener('storage', (event) => {
    if (event.key === 'chatList') {
      syncLocalStorage();
    }
  });
};
