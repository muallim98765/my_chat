import Vue from "vue";
import Antd from "ant-design-vue/lib";
import { FormModel } from "ant-design-vue";

Vue.use(FormModel);
Vue.use(Antd);
